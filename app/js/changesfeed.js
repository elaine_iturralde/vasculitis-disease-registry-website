'use strict';

angular.module('myApp.changesfeed', ['angularUtils.directives.dirPagination', 'ngMaterial'])

.controller('changesCtrl', function($scope, couchdb, Authorization, $mdDialog, $mdMedia){
	$scope.currentPage = 1;
	$scope.pageSize = 10;
	$scope.changes = [];
	var docs = [];
	var patient = {};

	if(Authorization.authorized == true){
		couchdb.db.getChanges(function(change){
			docs = change.results;
			$scope.length = docs.length;

			var i = 0;
			for(i = 0; i < $scope.length; i++){
				//if may doctor na naginput
				var d = new Date(docs[i].doc.timestamp);
				var formattedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
				var hours = (d.getHours() < 10) ? "0" + d.getHours() : d.getHours();
				var minutes = (d.getMinutes() < 10) ? "0" + d.getMinutes() : d.getMinutes();
				var formattedTime = hours + ":" + minutes;

				patient = {
					firstname: docs[i].doc.firstname,
					lastname: docs[i].doc.lastname,
					id: 'patientid',
					seq: docs[i].seq,
					date: formattedDate,
					time: formattedTime,
					doctor: 'input doctor here',
					_id: docs[i].doc._id
					// doctor: docs[i].changes.doc.doctor,
				};
				$scope.changes.splice(0,0,patient);
			}
		})
	}

	$scope.getRevision = function(id, event) {

		var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
	    $mdDialog.show({
	      controller: 'patientCtrl',
	      templateUrl: 'templates/patient/patient.html',
	      parent: angular.element(document.body),
	      targetEvent: event,
	      clickOutsideToClose:true,
	      fullscreen: useFullScreen,
	      resolve: {
		      id: function () {
		        return id;
		      }
		    }
	    })
	    .then(function(answer) {
	      $scope.status = 'You said the information was "' + answer + '".';
	    }, function() {
	      $scope.status = 'You cancelled the dialog.';
	    });
	    $scope.$watch(function() {
	      return $mdMedia('xs') || $mdMedia('sm');
	    }, function(wantsFullScreen) {
	      $scope.customFullscreen = (wantsFullScreen === true);
	    });
	}
})

.controller('patientCtrl', function($scope, id, Authorization, couchdb, $mdDialog){

	$scope.rev_no = "";
	if(Authorization.authorized == true){
		couchdb.doc.get(id, function(result){
			//if may doctor na naginput
			var d = new Date(result.timestamp);
			var formattedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();
			var hours = (d.getHours() < 10) ? "0" + d.getHours() : d.getHours();
			var minutes = (d.getMinutes() < 10) ? "0" + d.getMinutes() : d.getMinutes();
			var formattedTime = hours + ":" + minutes;

			$scope.patient = 
			{ 
			  "id": id,
		      "_id": result._id,
		      "_rev": result._rev,
		      "timestamp": result.timestamp,
		      "firstname": result.firstname, 
		      "lastname": result.lastname, 
		      "sex": result.sex,
		      "caseno": result.caseno,
		      "md": result.md,
		      "institution": result.institution,
		      "contact": result.contact,
		      "email": result.email,
		      "address": result.address,
		      "status": result.status,
		      "date_registryentry": result.date_registryentry,
		      "date_diagnosis": result.date_diagnosis,
		      "date_onset": result.date_onset,
		      "diseaseduration": result.diseaseduration,
		      "chiefcomplaint": result.chiefcomplaint,
		      "civilstatus": result.civilstatus,
		      "years_education": result.years_education,
		      "employment": result.employment,
		      "smoking": result.smoking,
		      "alcohol": result.alcohol,
		      "date_birth": result.date_birth,
		      "expired": result.expired,
		      "expired_cause": result.expired_cause,
		      "family": result.family,
		      "gluco_rec": result.gluco_rec,
		      "gluco_currrec": result.gluco_currrec,
		      "gluco_lasttaken": result.gluco_lasttaken,
		      "gluco_months": result.gluco_months,
		      "ivcyclo_rec": result.ivcyclo_rec,
		      "ivcyclo_currrec": result.ivcyclo_currrec,
		      "ivcyclo_lasttaken": result.ivcyclo_lasttaken,
		      "ivcyclo_months": result.ivcyclo_months,
		      "pocyclo_rec": result.pocyclo_rec,
		      "pocyclo_currrec": result.pocyclo_currrec,
		      "pocyclo_lasttaken": result.pocyclo_lasttaken,
		      "pocyclo_months": result.pocyclo_months,
		      "aza_rec": result.aza_rec,
		      "aza_currrec": result.aza_currrec,
		      "aza_lasttaken": result.aza_lasttaken,
		      "aza_months": result.aza_months,
		      "metho_rec": result.metho_rec,
		      "metho_currrec": result.metho_currrec,
		      "metho_lasttaken": result.metho_lasttaken,
		      "metho_months": result.metho_months,
		      "myco_rec": result.myco_rec,
		      "myco_currrec": result.myco_currrec,
		      "myco_lasttaken": result.myco_lasttaken,
		      "myco_months": result.myco_months,
		      "cyclo_rec": result.cyclo_rec,
		      "cyclo_currrec": result.cyclo_currrec,
		      "cyclo_lasttaken": result.cyclo_lasttaken,
		      "cyclo_months": result.cyclo_months,
		      "eta_rec": result.eta_rec,
		      "eta_currrec": result.eta_currrec,
		      "eta_lasttaken": result.eta_lasttaken, 
		      "eta_months": result.eta_months,
		      "infli_rec": result.infli_rec,
		      "infli_currrec": result.infli_currrec,
		      "infli_lasttaken": result.infli_lasttaken,
		      "infli_months": result.infli_months,
		      "ada_rec": result.ada_rec,
		      "ada_currrec": result.ada_currrec,
		      "ada_lasttaken": result.ada_lasttaken,
		      "ada_months": result.ada_months,
		      "ritu_rec": result.ritu_rec,
		      "ritu_currrec": result.ritu_currrec,
		      "ritu_lasttaken": result.ritu_lasttaken,
		      "ritu_months": result.ritu_months,
		      "plasma_rec": result.plasma_rec,
		      "plasma_currrec": result.plasma_currrec,
		      "plasma_lasttaken": result.plasma_lasttaken,
		      "plasma_months": result.plasma_months,
		      "tmphd_rec": result.tmphd_rec,
		      "tmphd_currrec": result.tmphd_currrec,
		      "tmphd_lasttaken": result.tmphd_lasttaken,
		      "tmphd_months": result.tmphd_months,
		      "tmpld_rec": result.tmpld_rec,
		      "tmpld_currrec": result.tmpld_currrec,
		      "tmpld_lasttaken": result.tmpld_lasttaken,
		      "tmpld_months": result.tmpld_months,
		      "aspld_rec": result.aspld_rec,
		      "aspld_currrec": result.aspld_currrec,
		      "aspld_lasttaken": result.aspld_months,
		      "asphd_rec": result.asphd_rec,
		      "asphd_currrec": result.asphd_currrec,
		      "asphd_lasttaken": result.asphd_lasttaken,
		      "asphd_months": result.asphd_months,
		      "clopi_rec": result.clopi_rec,
		      "clopi_currrec": result.clopi_currrec,
		      "clopi_lasttaken": result.clopi_lasttaken,
		      "clopi_months": result.clopi_months,
		      "stat_rec": result.stat_rec,
		      "stat_currrec": result.stat_currrec,
		      "stat_lasttaken": result.stat_lasttaken,
		      "stat_months": result.stat_months,
		      "oral_rec": result.oral_rec,
		      "oral_currrec": result.oral_currrec,
		      "oral_lasttaken": result.oral_lasttaken,
		      "oral_months": result.oral_months,
		      "est_rec": result.est_rec,
		      "est_currrec": result.est_currrec,
		      "est_lasttaken": result.est_lasttaken,
		      "est_months": result.est_months,
		      "other_name": result.other_name,
		      "other_rec": result.other_rec,
		      "other_currrec": result.other_currrec,
		      "other_lasttaken": result.other_lasttaken,
		      "other_months": result.other_months,
		      "compli_aneurysm": result.compli_aneurysm,
		      "compli_stroke": result.compli_stroke,
		      "compli_limb": result.compli_limb,
		      "compli_dialysis": result.compli_dialysis,
		      "compli_others": result.compli_others,
		      "inva_ptca": result.inva_ptca,
		      "inva_aneurysm": result.inva_aneurysm,
		      "inva_ampu": result.inva_ampu,
		      "inva_others": result.inva_others
		    }

		    $scope.selectedItem = 1;
		    $scope.rev_nos = [];
		    var i = 0;
		    var changes_length = result.changes.length;
		    for(i = 0; i < changes_length-1; i++){
		    	$scope.rev_nos.push(i+1);
		    }

		    $scope.getSelectedNumber = function(){
		    	$scope.hide_prevcard = true;
			    if(changes_length != 1){
			    	$scope.hide_prevcard = false;
					$scope.patient_history = 
					{ 
					  "id": id,
				      "_id": result.changes[$scope.selectedItem-1]._id,
				      "_rev": result.changes[$scope.selectedItem-1]._rev,
				      "timestamp": result.changes[$scope.selectedItem-1].timestamp,
				      "firstname": result.changes[$scope.selectedItem-1].firstname, 
				      "lastname": result.changes[$scope.selectedItem-1].lastname, 
				      "sex": result.changes[$scope.selectedItem-1].sex,
				      "caseno": result.changes[$scope.selectedItem-1].caseno,
				      "md": result.changes[$scope.selectedItem-1].md,
				      "institution": result.changes[$scope.selectedItem-1].institution,
				      "contact": result.changes[$scope.selectedItem-1].contact,
				      "email": result.changes[$scope.selectedItem-1].email,
				      "address": result.changes[$scope.selectedItem-1].address,
				      "status": result.changes[$scope.selectedItem-1].status,
				      "date_registryentry": result.changes[$scope.selectedItem-1].date_registryentry,
				      "date_diagnosis": result.changes[$scope.selectedItem-1].date_diagnosis,
				      "date_onset": result.changes[$scope.selectedItem-1].date_onset,
				      "diseaseduration": result.changes[$scope.selectedItem-1].diseaseduration,
				      "chiefcomplaint": result.changes[$scope.selectedItem-1].chiefcomplaint,
				      "civilstatus": result.changes[$scope.selectedItem-1].civilstatus,
				      "years_education": result.changes[$scope.selectedItem-1].years_education,
				      "employment": result.changes[$scope.selectedItem-1].employment,
				      "smoking": result.changes[$scope.selectedItem-1].smoking,
				      "alcohol": result.changes[$scope.selectedItem-1].alcohol,
				      "date_birth": result.changes[$scope.selectedItem-1].date_birth,
				      "expired": result.changes[$scope.selectedItem-1].expired,
				      "expired_cause": result.changes[$scope.selectedItem-1].expired_cause,
				      "family": result.changes[$scope.selectedItem-1].family,
				      "gluco_rec": result.changes[$scope.selectedItem-1].gluco_rec,
				      "gluco_currrec": result.changes[$scope.selectedItem-1].gluco_currrec,
				      "gluco_lasttaken": result.changes[$scope.selectedItem-1].gluco_lasttaken,
				      "gluco_months": result.changes[$scope.selectedItem-1].gluco_months,
				      "ivcyclo_rec": result.changes[$scope.selectedItem-1].ivcyclo_rec,
				      "ivcyclo_currrec": result.changes[$scope.selectedItem-1].ivcyclo_currrec,
				      "ivcyclo_lasttaken": result.changes[$scope.selectedItem-1].ivcyclo_lasttaken,
				      "ivcyclo_months": result.changes[$scope.selectedItem-1].ivcyclo_months,
				      "pocyclo_rec": result.changes[$scope.selectedItem-1].pocyclo_rec,
				      "pocyclo_currrec": result.changes[$scope.selectedItem-1].pocyclo_currrec,
				      "pocyclo_lasttaken": result.changes[$scope.selectedItem-1].pocyclo_lasttaken,
				      "pocyclo_months": result.changes[$scope.selectedItem-1].pocyclo_months,
				      "aza_rec": result.changes[$scope.selectedItem-1].aza_rec,
				      "aza_currrec": result.changes[$scope.selectedItem-1].aza_currrec,
				      "aza_lasttaken": result.changes[$scope.selectedItem-1].aza_lasttaken,
				      "aza_months": result.changes[$scope.selectedItem-1].aza_months,
				      "metho_rec": result.changes[$scope.selectedItem-1].metho_rec,
				      "metho_currrec": result.changes[$scope.selectedItem-1].metho_currrec,
				      "metho_lasttaken": result.changes[$scope.selectedItem-1].metho_lasttaken,
				      "metho_months": result.changes[$scope.selectedItem-1].metho_months,
				      "myco_rec": result.changes[$scope.selectedItem-1].myco_rec,
				      "myco_currrec": result.changes[$scope.selectedItem-1].myco_currrec,
				      "myco_lasttaken": result.changes[$scope.selectedItem-1].myco_lasttaken,
				      "myco_months": result.changes[$scope.selectedItem-1].myco_months,
				      "cyclo_rec": result.changes[$scope.selectedItem-1].cyclo_rec,
				      "cyclo_currrec": result.changes[$scope.selectedItem-1].cyclo_currrec,
				      "cyclo_lasttaken": result.changes[$scope.selectedItem-1].cyclo_lasttaken,
				      "cyclo_months": result.changes[$scope.selectedItem-1].cyclo_months,
				      "eta_rec": result.changes[$scope.selectedItem-1].eta_rec,
				      "eta_currrec": result.changes[$scope.selectedItem-1].eta_currrec,
				      "eta_lasttaken": result.changes[$scope.selectedItem-1].eta_lasttaken, 
				      "eta_months": result.changes[$scope.selectedItem-1].eta_months,
				      "infli_rec": result.changes[$scope.selectedItem-1].infli_rec,
				      "infli_currrec": result.changes[$scope.selectedItem-1].infli_currrec,
				      "infli_lasttaken": result.changes[$scope.selectedItem-1].infli_lasttaken,
				      "infli_months": result.changes[$scope.selectedItem-1].infli_months,
				      "ada_rec": result.changes[$scope.selectedItem-1].ada_rec,
				      "ada_currrec": result.changes[$scope.selectedItem-1].ada_currrec,
				      "ada_lasttaken": result.changes[$scope.selectedItem-1].ada_lasttaken,
				      "ada_months": result.changes[$scope.selectedItem-1].ada_months,
				      "ritu_rec": result.changes[$scope.selectedItem-1].ritu_rec,
				      "ritu_currrec": result.changes[$scope.selectedItem-1].ritu_currrec,
				      "ritu_lasttaken": result.changes[$scope.selectedItem-1].ritu_lasttaken,
				      "ritu_months": result.changes[$scope.selectedItem-1].ritu_months,
				      "plasma_rec": result.changes[$scope.selectedItem-1].plasma_rec,
				      "plasma_currrec": result.changes[$scope.selectedItem-1].plasma_currrec,
				      "plasma_lasttaken": result.changes[$scope.selectedItem-1].plasma_lasttaken,
				      "plasma_months": result.changes[$scope.selectedItem-1].plasma_months,
				      "tmphd_rec": result.changes[$scope.selectedItem-1].tmphd_rec,
				      "tmphd_currrec": result.changes[$scope.selectedItem-1].tmphd_currrec,
				      "tmphd_lasttaken": result.changes[$scope.selectedItem-1].tmphd_lasttaken,
				      "tmphd_months": result.changes[$scope.selectedItem-1].tmphd_months,
				      "tmpld_rec": result.changes[$scope.selectedItem-1].tmpld_rec,
				      "tmpld_currrec": result.changes[$scope.selectedItem-1].tmpld_currrec,
				      "tmpld_lasttaken": result.changes[$scope.selectedItem-1].tmpld_lasttaken,
				      "tmpld_months": result.changes[$scope.selectedItem-1].tmpld_months,
				      "aspld_rec": result.changes[$scope.selectedItem-1].aspld_rec,
				      "aspld_currrec": result.changes[$scope.selectedItem-1].aspld_currrec,
				      "aspld_lasttaken": result.changes[$scope.selectedItem-1].aspld_months,
				      "asphd_rec": result.changes[$scope.selectedItem-1].asphd_rec,
				      "asphd_currrec": result.changes[$scope.selectedItem-1].asphd_currrec,
				      "asphd_lasttaken": result.changes[$scope.selectedItem-1].asphd_lasttaken,
				      "asphd_months": result.changes[$scope.selectedItem-1].asphd_months,
				      "clopi_rec": result.changes[$scope.selectedItem-1].clopi_rec,
				      "clopi_currrec": result.changes[$scope.selectedItem-1].clopi_currrec,
				      "clopi_lasttaken": result.changes[$scope.selectedItem-1].clopi_lasttaken,
				      "clopi_months": result.changes[$scope.selectedItem-1].clopi_months,
				      "stat_rec": result.changes[$scope.selectedItem-1].stat_rec,
				      "stat_currrec": result.changes[$scope.selectedItem-1].stat_currrec,
				      "stat_lasttaken": result.changes[$scope.selectedItem-1].stat_lasttaken,
				      "stat_months": result.changes[$scope.selectedItem-1].stat_months,
				      "oral_rec": result.changes[$scope.selectedItem-1].oral_rec,
				      "oral_currrec": result.changes[$scope.selectedItem-1].oral_currrec,
				      "oral_lasttaken": result.changes[$scope.selectedItem-1].oral_lasttaken,
				      "oral_months": result.changes[$scope.selectedItem-1].oral_months,
				      "est_rec": result.changes[$scope.selectedItem-1].est_rec,
				      "est_currrec": result.changes[$scope.selectedItem-1].est_currrec,
				      "est_lasttaken": result.changes[$scope.selectedItem-1].est_lasttaken,
				      "est_months": result.changes[$scope.selectedItem-1].est_months,
				      "other_name": result.changes[$scope.selectedItem-1].other_name,
				      "other_rec": result.changes[$scope.selectedItem-1].other_rec,
				      "other_currrec": result.changes[$scope.selectedItem-1].other_currrec,
				      "other_lasttaken": result.changes[$scope.selectedItem-1].other_lasttaken,
				      "other_months": result.changes[$scope.selectedItem-1].other_months,
				      "compli_aneurysm": result.changes[$scope.selectedItem-1].compli_aneurysm,
				      "compli_stroke": result.changes[$scope.selectedItem-1].compli_stroke,
				      "compli_limb": result.changes[$scope.selectedItem-1].compli_limb,
				      "compli_dialysis": result.changes[$scope.selectedItem-1].compli_dialysis,
				      "compli_others": result.changes[$scope.selectedItem-1].compli_others,
				      "inva_ptca": result.changes[$scope.selectedItem-1].inva_ptca,
				      "inva_aneurysm": result.changes[$scope.selectedItem-1].inva_aneurysm,
				      "inva_ampu": result.changes[$scope.selectedItem-1].inva_ampu,
				      "inva_others": result.changes[$scope.selectedItem-1].inva_others
				    }
				}
		    }

		    
		})

		$scope.cancel = function() {
		  $mdDialog.cancel();
		};
	}
})