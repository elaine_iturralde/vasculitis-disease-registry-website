'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'ui.router', 
  'ngResource', 
  'ngFileSaver', 
  'ui.bootstrap', 
  'ngCookies', 
  'myApp.about',
  'myApp.dashboard',
  'myApp.changesfeed',
  'myApp.database',
  'myApp.home',
  'myApp.login', 
  'myApp.manageusers',
  'myApp.simplecouch',
  'underscore',
  'authorization',
])