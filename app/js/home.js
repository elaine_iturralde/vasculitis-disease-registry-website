'use strict';

angular.module('myApp.home', [])

.config(function ($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise('home')

  $stateProvider
  	.state("home", {
  	  url: "/home",
	  templateUrl: 'templates/home/home.html'
	})
})