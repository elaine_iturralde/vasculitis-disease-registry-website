angular.module('myApp.simplecouch',[])

.service('Authorization', ['$state', function($state) {

  this.authorized = false,
  this.memorizedState = null;

  var
  clear = function() {
    this.authorized = false;
    this.memorizedState = null;
  },

  go = function(fallback) {
    this.authorized = true;
    var targetState = this.memorizedState ? this.memorizedState : fallback;
    $state.go(targetState);
  };

  return {
    authorized: this.authorized,
    memorizedState: this.memorizedState,
    clear: clear,
    go: go
  };
}])

.factory('AuthenticationService',
    ['Base64', '$http', '$cookieStore', '$rootScope', '$timeout',
    function (Base64, $http, $cookieStore, $rootScope, $timeout) {
        var service = {};
  
        service.SetCredentials = function (username, password) {
            var authdata = Base64.encode(username + ':' + password);
  
            $rootScope.globals = {
                currentUser: {
                    username: username,
                    authdata: authdata
                }
            };
  
            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata; // jshint ignore:line
            $cookieStore.put('globals', $rootScope.globals);
        };
  
        service.ClearCredentials = function () {
            $rootScope.globals = {};
            $cookieStore.remove('globals');
            $http.defaults.headers.common.Authorization = 'Basic ';
        };
  
        return service;
    }])
  
.factory('Base64', function () {
    /* jshint ignore:start */
  
    var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
  
    return {
        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
  
            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
  
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
  
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
  
                output = output +
                    keyStr.charAt(enc1) +
                    keyStr.charAt(enc2) +
                    keyStr.charAt(enc3) +
                    keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);
  
            return output;
        },
  
        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
  
            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                window.alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
  
            do {
                enc1 = keyStr.indexOf(input.charAt(i++));
                enc2 = keyStr.indexOf(input.charAt(i++));
                enc3 = keyStr.indexOf(input.charAt(i++));
                enc4 = keyStr.indexOf(input.charAt(i++));
  
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
  
                output = output + String.fromCharCode(chr1);
  
                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }
  
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
  
            } while (i < input.length);
  
            return output;
        }
    };
  
    /* jshint ignore:end */
})

.provider('couchConfig', {
    // internal configuration data; configured through setter function
    serverUrl: null,
    db : null,
    method : 'GET',
    usrCtx : { name: null, roles: [], password: null },
    // configuration method for setting the server url
    setServer: function(serverUrl) {
        this.serverUrl = serverUrl;
    },
    // set the dbName
    setDB : function (db) {
        this.db = db;
    },

    $get: function() {
        // return the service instance
        return {
            server : this.serverUrl,
            db : this.db,
            method : this.method,
            usrCtx : this.usrCtx
        };
    }
})

.config(function (couchConfigProvider,$httpProvider) {
    couchConfigProvider.setServer('http://127.0.0.1:5984');
    couchConfigProvider.setDB('legit');
})

.factory('couchdb', function (couchConfig, $http, Base64) {

    var extendJSONP = function (config) {

        if (config.method === "JSONP")
            if (config.params)
                config.params.callback = "JSON_CALLBACK";
            else
                config.params = { callback: "JSON_CALLBACK" };

        return config;
    }

    var encodeUri = function (base, part1, part2) {
        var uri = base;
        if (part1) uri = uri + "/" + encodeURIComponent(part1);
        if (part2) uri = uri + "/" + encodeURIComponent(part2);
        return uri.replace('%2F', '/');
    };

    var executeQuery = function (config, cb) {
        return $http(config).success(function (data, status) {
            if (data.rows === undefined) {
                cb(data);
            } else {
                cb(data.rows);
            }
        });
    };

    var getDbUri = function () {
        return encodeUri(couchConfig.server, couchConfig.db);
    };

    var setAuthorization = function(){
        var username = couchConfig.usrCtx.name;
        var password = couchConfig.usrCtx.password;

        $http.defaults.headers.common['Authorization'] = 'Basic ' + Base64.encode(username + ':' + password);
    }

    return {
        //private method
        _queryView: function (viewURL, qparams, cb) {
            var config = {
                method: couchConfig.method,
                url: getDbUri() + viewURL
            };

            if (qparams) {
                // Raise limit by 1 for pagination
                if (qparams.limit) qparams.limit++;
                // Convert key parameters to JSON
                for (p in qparams) switch (p) {
                    case "key":
                    case "keys":
                    case "startkey":
                    case "endkey":
                        qparams[p] = angular.toJson(qparams[p]);
                }
                config.params = qparams;
            }

            return executeQuery(extendJSONP(config), cb);
        },

        config: {
            getServer: function () {
                return couchConfig.server;
            },
            setServer: function (url) {
                couchConfig.server = url;
            },
            setMethod: function (method) {
                couchConfig.method = method;
            },
            getMethod: function () {
                return couchConfig.method;
            }
        },
        db: {
            use: function (dbName) {
                couchConfig.db = dbName;
            },
            getName: function () {
                return couchConfig.db;
            },
            getAllDocs: function (cb) {

                setAuthorization();
                return $http({
                    method: "GET",
                    url: getDbUri() + "/_all_docs",
                    headers: {'Content-Type': 'application/json'}
                })
                .success(function (data) {
                    return cb(data);
                });
            },
            getChanges: function (cb) {
                setAuthorization();
                return $http({
                    method: "GET",
                    url: getDbUri() + "/_changes",
                    headers: {'Content-Type': 'application/json'},
                    params: {'include_docs': true, 'style': 'all_docs'}
                })
                .success(function(data){
                    return cb(data);
                })
            }
        },
        // all doc manipulation
        doc: {
            post: function (data, cb) {
                setAuthorization();
                return $http({
                    method: "POST",
                    url: getDbUri(),
                    data: data,
                    headers: {'Content-Type': 'application/json'}
                })
                .success(function (data, status) {
                    cb(data);
                });

            },
            delete: function (doc, cb) {
                setAuthorization();
                return $http({
                    method: "DELETE",
                    url: encodeUri(getDbUri(), doc._id),
                    params: { rev: doc._rev }
                })
                .success(function (data, status) {
                    cb(data);
                });
            },
            get: function (id, cb) {
                var config = {
                    method: couchConfig.method,
                    url: encodeUri(getDbUri(), id),
                    params: { revs: true, revs_info: true }
                };
                setAuthorization();
                return $http(extendJSONP(config)).success(function (data, status) {
                    cb(data);
                });
            },
            getWithRevision: function (id, rev, cb) {
                var config = {
                    method: couchConfig.method,
                    url: encodeUri(getDbUri(), id),
                    params: { rev: rev }
                };
                setAuthorization();
                return $http(extendJSONP(config)).success(function (data, status) {
                    cb(data);
                });
            },
            put: function (data, cb) {
                setAuthorization();
                return $http({
                    method: "PUT",
                    url: encodeUri(getDbUri(), data._id),
                    data: data,
                    headers: {'Content-Type': 'application/json'}
                }).success(function (data, status) {
                    cb(data);
                });
            }
        },
        // attachments manipulation
        attach: {
            put: function (doc, file, options, cb) {
                setAuthorization();
                return $http({
                    method: "PUT",
                    url: encodeUri(getDbUri(), doc._id, options.name || file.name),
                    params: { rev: doc._rev },
                    headers: { "Content-Type": options.fileType || file.type },
                    data: file
                }).success(function (data, status) {
                        cb(data);
                    });
            },
            get: function (id, name, cb) {
                setAuthorization();
                return $http({
                    method: "GET",
                    url: encodeUri(getDbUri(), id, name)
                }).success(function (data, status) {
                        cb(data);
                    });
            },
            delete: function (doc, name, cb) {
                setAuthorization();
                return $http({
                    method: "DELETE",
                    url: encodeUri(getDbUri(), id, name),
                    params: { rev: doc._rev }
                }).success(function (data, status) {
                        cb(data);
                    });
            }
        },
        // user part
        user: {
            session: function (cb) {
                var server = this;
                setAuthorization();
                return $http({
                    method: "GET",
                    url: encodeUri(couchConfig.server) + "/_session"
                })
                .success(function (data) {
                    couchConfig.usrCtx = data.userCtx;
                    cb(couchConfig.usrCtx);
                });
            },
            create: function (userName, password, roles, found, cb) {
                var userData = "";
                if(found == ""){
                    userData = {
                        _id: 'org.couchdb.user:' + userName,
                        name: userName,
                        password: password,
                        roles: [],
                        type: 'user'
                    };
                    setAuthorization();
                    return $http({
                        method: "POST",
                        url: encodeUri(couchConfig.server) + "/_users",
                        data: userData
                    }).success(function (data) {
                        cb(data);
                    }).catch(function (err){
                        console.log(err);
                    });
                }
                else{
                    var rev = "";
                    setAuthorization();
                    $http({
                        method: "PUT",
                        url: encodeUri(couchConfig.server) + "/_users",
                        params: { "org.couchdb.user": userName }
                    })
                    .success(function (data) {
                        userData ={
                            name: userName,
                            password: password,
                            roles: [],
                            type: 'user'
                        }
                        rev = data._rev;
                    });
                    setAuthorization();
                    return $http({
                        method: method,
                        url: encodeUri(couchConfig.server) + "/_users/org.couchdb.user:" + userName,
                        data: userData,
                        headers: 
                            {
                                'Content-Type': 'application/json',
                                'Accept': 'application/json',
                                'If-Match': rev
                            }
                    }).success(function (data) {
                        cb(data);
                    }).catch(function (err){
                        console.log(err);
                    });
                }
            },
            getAllUsers: function (cb) {
                setAuthorization();
                return $http({
                    method: "GET",
                    url: encodeUri(couchConfig.server) + "/_users/_all_docs",
                    headers: {'Content-Type': 'application/json'}
                })
                .success(function (data) {
                    return cb(data);
                });
            },
            login: function (usr, pwd, cb) {
                var body =
                    "name=" + encodeURIComponent(usr) +
                        "&password=" + encodeURIComponent(pwd);

                return $http({
                    method: "POST",
                    url: encodeUri(couchConfig.server) + "/_session",
                    headers: { "Content-Type": "application/x-www-form-urlencoded" },
                    data: body.replace(/%20/g, "+")
                }).success(function (data, status, header, config) {

                    delete data["ok"];
                    data.password = pwd
                    data.name = usr;
                    couchConfig.usrCtx = data;
                    cb(data);

                });
            },
            isAuthenticated: function (cb) {

                var server = this;
                return $http({
                    method: "GET",
                    url: encodeUri(couchConfig.server) + "/_session"
                })
                .success(function (data) {
                    if (data.name === null) {
                        cb(false);
                    } else {
                        cb(true);
                    }

                });
            },
            get: function () {
                return couchConfig.usrCtx;
            },
            logout: function (cb) {
                var server = this;
                $http.defaults.headers.common.Authorization = 'Basic ';
                return $http({
                    method: "DELETE",
                    url: encodeUri(couchConfig.server) + "/_session"
                })
                .success(function () {
                    couchConfig.usrCtx = { name: null, roles: [] };
                    cb(true);
                });
            }
        },
        //query view
        view: function (design, view, qparams, cb) {
            setAuthorization();
            return this._queryView(
                "/_design/" + encodeURIComponent(design) +
                    "/_view/" + encodeURIComponent(view),
                qparams,
                cb
            );
        },
        // home made query
        query: function (config, cb) {
            return executeQuery(extendJSONP(config), cb);
        }

    }

})
