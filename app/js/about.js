'use strict';

angular.module('myApp.about', [])

.config(function ($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise('about')

  $stateProvider
  	.state('aboutpgh', {
  	  url: '/aboutpgh',
	  templateUrl: 'templates/about/aboutpgh.html'
	})
	.state('aboutvdr', {
  	  url: '/aboutvdr',
	  templateUrl: 'templates/about/aboutvdr.html'
	})
})