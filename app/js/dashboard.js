'use strict';

angular.module('myApp.dashboard', ['chart.js'])

.controller('barCtrl', function ($scope, Authorization, couchdb) {
  $scope.gender = ['Female', 'Male', 'Missing Data'];
  $scope.civilstatus = ['Single/Widower/Separated', 'Married/Live-In Partner', 'Missing Data'];
  $scope.status = ['Active', 'Lost to Followup', 'Expired', 'Missing Data'];
  $scope.employment = ['No Source of Income', 'Gainfully Employed', 'Missing Data'];
  $scope.smoking = ['No', 'Rare', 'Often', 'Missing Data'];
  $scope.alcohol = ['No', 'Rare', 'Often', 'Missing Data'];
  $scope.gluco = {
  	ever_received: ['Yes', 'No', 'Unknown', 'Missing Data'],
  	curr_receiving: ['Yes', 'No', 'Unknown', 'Missing Data'],
  	last_taken: ['Today', 'The Week Before Consultation', 'Greater than One Week Before Consultation', 'Missing Data']
  }
  $scope.fields = [
  		'Gender', 
  		'Civil Status', 
  		'Status', 
  		'Employment', 
  		'Smoking', 
  		'Alcohol' 
  		// 'Prednisone or Other Glucocorticoids', 
  		// 'IV Cyclophosphamide',
  		// 'PO Cyclophosphamide',
  		// 'Azathioprine',
  		// 'Methotrexate',
  		// 'Mycophenolate',
  		// 'Cyclosporine',
  		// 'Etanercept',
  		// 'Infliximab',
  		// 'Adalimumab',
  		// 'Rituximab',
  		// 'Plasmapheresis',
  		// 'TMP/SMX: High-Dose (ds tablet BID)',
  		// 'TMP/SMX: Low-Dose (ss tablet OD or ds tablet 3x/week)',
  		// 'Aspirin: Low-Dose',
  		// 'Aspirin: High-Dose (>325mg/day)',
  		// 'Clopidogrel',
  		// 'Statin',
  		// 'Oral Contraceptives',
  		// 'Estrogen',
  		// 'Others'
  ];
  $scope.selectedItem = 'Gender';
  $scope.selectedGraph = 'Bar Graph';
  $scope.graphs = ['Bar Graph', 'Pie Chart'];
  $scope.percentage = false;

  $scope.bardata = [
    [0,0,0]
  ];
  $scope.piedata = [0,0,0];

  $scope.total_docs = 0;

  $scope.getSelectedGraph = function(){

  	if($scope.selectedGraph == 'Pie Chart'){
  		$scope.percentage = true;
  	}
  	else{
  		$scope.percentage = false;
  	}
  }

  $scope.getSelectedText = function(){

  	var ids = [];
  	var allDocs = [];
  	var count1 = 0;
  	var count2 = 0;
  	var count3 = 0;
  	var count4 = 0;
  	
  	if(Authorization.authorized == true){
  		
  		couchdb.db.getAllDocs(function(data, result){
          	allDocs = data;
          	$scope.total_docs = allDocs.total_rows;

          	var ids = [];
	        var i = 0;
	        for(var i = 0; i < allDocs.total_rows; i++){
	        	ids.push($scope.allDocs.rows[i].id);
	        }

	        for(var i = 0; i < allDocs.total_rows; i++){
	        	var id = ids[i];
	        	couchdb.doc.get(id, function(result){
	        	if(
	        			($scope.selectedItem == 'Civil Status' && result.civilstatus == "Single/Widower/Separated") ||
	        			($scope.selectedItem == 'Status' && result.status == 'Active') ||
	        			($scope.selectedItem == 'Employment' && result.employment == 'No Source of Income') ||
	        			($scope.selectedItem == 'Smoking' && result.smoking == 'No') ||
	        			($scope.selectedItem == 'Alcohol' && result.alcohol == 'No') ||
	        			($scope.selectedItem == 'Gender' && result.sex == 'Female')
	        		){
			  			count1 += 1;
			  		}
			  		else if(
			  			($scope.selectedItem == 'Civil Status' && result.civilstatus == 'Married/Live-In Partner') ||
			  			($scope.selectedItem == 'Status' && result.status == 'Lost to Followup') ||
			  			($scope.selectedItem == 'Employment' && result.employment == 'Gainfully Employed') ||
			  			($scope.selectedItem == 'Smoking' && result.smoking == 'Rare') ||
			  			($scope.selectedItem == 'Alcohol' && result.alcohol == 'Rare') ||
			  			($scope.selectedItem == 'Gender' && result.sex == 'Male')
			  		){
			  			count2 += 1;
			  		}
			  		else if(
			  			($scope.selectedItem == 'Status' && result.status == 'Expired') ||
			  			($scope.selectedItem == 'Smoking' && result.smoking == 'Often') ||
			  			($scope.selectedItem == 'Alcohol' && result.alcohol == 'Often')
			  		){
				  		count3 += 1;
				  	}

	        		if($scope.selectedItem == 'Civil Status'){
			  			$scope.labels = $scope.civilstatus;
				  		count3 = $scope.total_docs - (count1+count2);
				  	}
				  	else if($scope.selectedItem == 'Status'){
				  		$scope.labels = $scope.status;
				  		count4 = $scope.total_docs - (count1+count2+count3);
				  	}
					else if($scope.selectedItem == 'Employment'){
				  		$scope.labels = $scope.employment;
				  		count3 = $scope.total_docs - (count1+count2);
				  	}	  	
				  	else if($scope.selectedItem == 'Smoking'){
				  		$scope.labels = $scope.smoking;
					  	count4 = $scope.total_docs - (count1+count2+count3);
				  	}
				  	else if($scope.selectedItem == 'Alcohol'){
				  		$scope.labels = $scope.alcohol;
				  		count4 = $scope.total_docs - (count1+count2+count3);
				  	}
				  	else{
				  		$scope.labels = $scope.gender;
					  	count3 = $scope.total_docs - (count1+count2);
				  	}
				  	$scope.bardata = [[count1, count2, count3, count4]];
				  	$scope.piedata = [
				  		count1/$scope.total_docs*100, 
				  		count2/$scope.total_docs*100, 
				  		count3/$scope.total_docs*100,
				  		count4/$scope.total_docs*100];
		        })
	        }
        })	       
  	}}
})


      