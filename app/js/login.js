var app = angular.module('myApp.login', ['ngMaterial'])

.controller('loginCtrl', ['$scope', '$mdDialog', '$state', '$mdToast', '$rootScope', 'Authorization', 'AuthenticationService', 'couchdb', function($scope, $mdDialog, $state, $mdToast, $rootScope, Authorization, AuthenticationService, couchdb) {

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.onLogin = function(username, password) {
    couchdb.user.login(username, password, function (admin){
      if(admin.roles[0] == "_admin"){
        couchdb.user.isAuthenticated(function (created){   
            $scope.dataLoading = true;
            if(created == true) {
              AuthenticationService.SetCredentials($scope.username, $scope.password);

              $scope.wrong = false;
              $mdDialog.hide();

              var last = {
                  bottom: false,
                  top: true,
                  left: false,
                  right: true
                };
              $scope.toastPosition = angular.extend({},last);
              $scope.getToastPosition = function() {
                sanitizePosition();
                return Object.keys($scope.toastPosition)
                  .filter(function(pos) { return $scope.toastPosition[pos]; })
                  .join(' ');
              };
              function sanitizePosition() {
                var current = $scope.toastPosition;
                if ( current.bottom && last.top ) current.top = false;
                if ( current.top && last.bottom ) current.bottom = false;
                if ( current.right && last.left ) current.left = false;
                if ( current.left && last.right ) current.right = false;
                last = angular.extend({},current);
              }
              var pinTo = $scope.getToastPosition();
                $mdToast.show(
                    $mdToast.simple()
                    .textContent('Successfully Logged In!')
                    .position(pinTo )
                    .hideDelay(3000)
                );

              Authorization.go('database');
              couchdb.db.getAllDocs(function(result){
                $rootScope.allDocs = result;
              });

              $rootScope.loggedin = true;

             } else {
              $scope.error = response.message;
              $scope.dataLoading = false;
            }
        })
      }
      else{
        $scope.wrong = true;
      }
    })
    .catch(function(){
      $scope.wrong = true;
    })
  }
}])

.controller('logoutCtrl', function($scope, $state, Authorization, AuthenticationService, couchdb,  $templateCache, $rootScope){
  
  $scope.onLogout = function() {
    $templateCache.removeAll();
    couchdb.user.logout(function(logout){
      $rootScope.loggedin = false;
      Authorization.clear();
      AuthenticationService.ClearCredentials();
    })
  }
})

.controller('loginModalCtrl', function ($scope, $mdDialog, $mdMedia)  {

  $scope.status = '  ';
  $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');

  $scope.showAdvanced = function(ev) {
    var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
    $mdDialog.show({
      controller: 'loginCtrl',
      templateUrl: 'templates/login/login.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: useFullScreen
    })
    $scope.$watch(function() {
      return $mdMedia('xs') || $mdMedia('sm');
    }, function(wantsFullScreen) {
      $scope.customFullscreen = (wantsFullScreen === true);
    });
  };
})