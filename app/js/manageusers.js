'use strict';

angular.module('myApp.manageusers', ['ngMaterial'])

.controller('manageUsersCtrl', function ($scope, couchdb, $mdDialog, $mdToast, Authorization, $filter) {

	$scope.wrong = false;
	$scope.selectedRole = "admin";
	$scope.roles = ["admin", "doctor"];

	$scope.users = [];
	if(Authorization.authorized == true){
		couchdb.user.getAllUsers(function(results){
			var i = 0;
			for(i = 1; i < results.total_rows; i++){
				$scope.users.push(results.rows[i].id.replace("org.couchdb.user:",""));
			}
		})
	}

	$scope.cancel = function() {
	   $mdDialog.cancel();
	};

	$scope.addUser = function(username, password, confirmpassword, role){

		var found = $filter('filter')($scope.users, username, true);
		if(
			password == confirmpassword && 
			username != "" && password != "" &&
			Authorization.authorized == true
		){
			$scope.wrong = false;
			var roles = [];
			roles.push(role);

			couchdb.user.create(username, password, roles, found, function(result){
				$mdDialog.hide();

	            var last = {
	                bottom: false,
	                top: true,
	                left: false,
	                right: true
	              };
	            $scope.toastPosition = angular.extend({},last);
	            $scope.getToastPosition = function() {
	              sanitizePosition();
	              return Object.keys($scope.toastPosition)
	                .filter(function(pos) { return $scope.toastPosition[pos]; })
	                .join(' ');
	            };
	            function sanitizePosition() {
	              var current = $scope.toastPosition;
	              if ( current.bottom && last.top ) current.top = false;
	              if ( current.top && last.bottom ) current.bottom = false;
	              if ( current.right && last.left ) current.left = false;
	              if ( current.left && last.right ) current.right = false;
	              last = angular.extend({},current);
	            }
	            var pinTo = $scope.getToastPosition();
	              $mdToast.show(
	                  $mdToast.simple()
	                  .textContent('Successfully Added New User!')
	                  .position(pinTo )
	                  .hideDelay(3000)
	              );
			})

		}
		else{
			$scope.wrong = true;
		}
	}	
})

.controller('addUserModalCtrl', function ($scope, $mdDialog, $mdMedia)  {

  $scope.status = '  ';
  $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');

  $scope.showAdvanced = function(ev) {
    var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
    $mdDialog.show({
      controller: 'manageUsersCtrl',
      templateUrl: 'templates/manageusers/edituser.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: useFullScreen
    })
    $scope.$watch(function() {
      return $mdMedia('xs') || $mdMedia('sm');
    }, function(wantsFullScreen) {
      $scope.customFullscreen = (wantsFullScreen === true);
    });
  };
})

.controller('editUserCtrl', function($scope, user){

	$scope.title = "Edit"
	$scope.user = 
	{
		username: user,
		password: "",
		confirmpassword: ""
	};

	$scope.cancel = function() {
	   $mdDialog.cancel();
	};
})

.controller('editUserModalCtrl', function ($scope, $mdDialog, $mdMedia)  {

  $scope.status = '  ';
  $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');

  $scope.showAdvanced = function(user, ev) {
    var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
    $mdDialog.show({
      controller: 'editUserCtrl',
      templateUrl: 'templates/manageusers/edituser.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: useFullScreen,
      resolve: {
	      user: function () {
	        return user;
	      }
	    }
    })
    $scope.$watch(function() {
      return $mdMedia('xs') || $mdMedia('sm');
    }, function(wantsFullScreen) {
      $scope.customFullscreen = (wantsFullScreen === true);
    });
  };
})