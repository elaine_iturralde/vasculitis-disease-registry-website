'use strict';

///change authorization to TRUE

angular.module('myApp.database', ['ngMaterial'])

.config(function ($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise('home')

  $stateProvider
  	.state('database', {
  	  url: '/database',
	  templateUrl: 'templates/database/database.html',
	  data:{
	      authorization: true,
	      redirectTo: 'home'
		}
	})
	.state('login', {
  	  url: '/login',
	  templateUrlUrl: 'templates/login/login.html',
      controller: 'loginCtrl'
	})
	.state('filterfields', {
  	  url: '/filterfields',
	  templateUrl: 'templates/filterfields/filterfields.html',
      data:{
	      authorization: true,
	      redirectTo: 'home'
		}
	})
})

.controller('SideNavCtrl', function ($scope, $timeout, $mdSidenav, $log) {
    $scope.toggleLeft = buildDelayedToggler('left');

    function debounce(func, wait, context) {
      var timer;
      return function debounced() {
        var context = $scope,
            args = Array.prototype.slice.call(arguments);
        $timeout.cancel(timer);
        timer = $timeout(function() {
          timer = undefined;
          func.apply(context, args);
        }, wait || 10);
      };
    }

    function buildDelayedToggler(navID) {
      return debounce(function() {
        $mdSidenav(navID)
          .toggle()
          .then(function () {
            $log.debug("toggle " + navID + " is done");
          });
      }, 200);
    }
    function buildToggler(navID) {
      return function() {
        $mdSidenav(navID)
          .toggle()
          .then(function () {
            $log.debug("toggle " + navID + " is done");
          });
      }
    }
})

.controller('LeftCtrl', function ($scope, $timeout, $mdSidenav, $log) {
	$scope.close = function () {
	  $mdSidenav('left').close()
	};
})

.controller('searchCtrl', function($scope, searchService, $mdDialog, couchdb){
	$scope.commonfields = searchService.getCommonFields();

	$scope.checkAll = function(item, check){
		for(var i in $scope.commonfields){
			if(item == $scope.commonfields[i].title){
				for(var j in $scope.commonfields[i].field){
					$scope.commonfields[i].field[j].checked = check;
				}
			}
		}
	}
})

.service('searchService', function($state, $resource, $q){

	return {
		commonfields: 
		[
			{
				title : 'General Details',
				field : 
					[ 
						{
							name: 'Patient ID', 
							checked: false,
						},
						{
							name: 'First Name',
							checked: false,
						},
						{
							name: 'Last Name',
							checked: false
						}
					],
				checked: false,
			},
			{
				title : 'Demographics',
				field : 
					[ 
						{
							name: 'Case Number', 
							checked: false,
						}, 
						{
							name: 'MD',
							checked: false,
						},
						{
							name: 'Institution',
							checked: false
						},
						{
							name: 'Contact Number', 
							checked: false,
						},
						{
							name: 'Email Address',
							checked: false,
						},
						{
							name: 'Sex',
							checked: false
						},
						{
							name: 'Civil Status', 
							checked: false,
						},
						{
							name: 'Date of Birth',
							checked: false,
						},
						{
							name: 'Date of First Symptom',
							checked: false
						},
						{
							name: 'Date of Diagnosis', 
							checked: false,
						},
						{
							name: 'Date of Registry Entry',
							checked: false,
						},
						{
							name: 'Disease Duration (cell M minus call L)',
							checked: false
						},
						{
							name: 'Status',
							checked: false
						},
						{
							name: 'If expired, indicate months from diagnosis', 
							checked: false,
						},
						{
							name: 'If expired, indicate cause of death (1 = infection; 2 = aneurysm rupture / leak; 3 = unrelated cause; 4 = others)',
							checked: false,
						},
						{
							name: 'Employment',
							checked: false
						},
						{
							name: 'Chief Complaint',
							checked: false
						}
					],
				checked: false,
			},
			{
				title : 'Family and Social Histories',
				field : 
					[ 
						{
							name: 'Family Member with Vasculitis (0 = none; 1 = 1 member; 2 = 2, and so on)', 
							checked: false,
						},
						{
							name: 'Smoking',
							checked: false,
						},
						{
							name: 'Alcohol',
							checked: false
						}
					],
				checked: false,
			},
			{
				title : 'Prednisone or Other Glucocorticoids',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'IV Cyclophosphamide',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'PO Cyclophosphamide',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'Azathioprine',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'Methotrexate',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'Mycophenolate',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'Cyclosporine',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'Etanercept',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'Infliximab',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'Adalimumab',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'Rituximab',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'Plasmapheresis',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'TMP/SMX: High-Dose (ds tablet BID)',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'TMP/SMX: Low-Dose (ss tablet OD or ds tablet)',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'Aspirin: Low-Dose',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'Aspirin: High-Dose (>325mg/day)s',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'Clopidogrel',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'Statin',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'Oral Contraceptives',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'Estrogen',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'Others',
				field:
					[
						{
							name: 'Ever Received',
							checked: false,
						},
						{
							name: 'Currently Receiving',
							checked: false,
						},
						{
							name: 'Last Taken',
							checked: false,
						},
						{
							name: 'Cummulative Months of Use',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'Complications',
				field:
					[
						{
							name: 'Aneurysm Rupture',
							checked: false,
						},
						{
							name: 'Stroke',
							checked: false,
						},
						{
							name: 'Limb Ischemia',
							checked: false,
						},
						{
							name: 'Dialysis',
							checked: false,
						},
						{
							name: 'Others',
							checked: false,
						}
					],
				checked: false,
			},
			{
				title : 'Invasive Management',
				field:
					[
						{
							name: 'PTCA',
							checked: false,
						},
						{
							name: 'Aneurysm Grafting',
							checked: false,
						},
						{
							name: 'Amputation',
							checked: false,
						},
						{
							name: 'Others',
							checked: false,
						}
					],
				checked: false,
			},
		],
		getCommonFields: function() {
          	return this.commonfields;
	      }
	}
})

.controller('excelCtrl', ['$scope', 'FileSaver', 'Blob', function($scope, FileSaver, Blob) {
  
	$scope.exportData = function(){
		var blob = new Blob([document.getElementById("exportable").innerHTML], {
		        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
		    })
		    FileSaver.saveAs(blob, "Report.xls")
	}
}])



